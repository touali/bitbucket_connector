<?php

namespace Drupal\bitbucket_connector\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a 'Pipelines Triggers' Block.
 *
 * @Block(
 *   id = "pipelines_block",
 *   admin_label = @Translation("Pipelines Triggers"),
 *   category = @Translation("Bitbucket Connector"),
 * )
 */
class PipelinesBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $renderArray = [
      '#theme' => 'pipelines_block',
      '#data' => []
    ];

    $prodUrl = Url::fromRoute('bitbucket_connector.publish', ['env' => 'prod']);

    if ($prodUrl->access()) {
      $prodLink = Link::fromTextAndUrl(t('Production'), $prodUrl);
      $renderArray['#data']['prod'] = $prodLink->toRenderable();
    }

    $preprodUrl = Url::fromRoute('bitbucket_connector.publish', ['env' => 'preprod']);

    if ($preprodUrl->access()) {
      $preprodLink = Link::fromTextAndUrl(t('Preproduction'), $preprodUrl);
      $renderArray['#data']['preprod'] = $preprodLink->toRenderable();
    }

    foreach ($renderArray['#data'] as $key => &$element) {
      $element['#attributes']['class'] = [
        "{$key}-trigger",
        "button",
        "button--primary",
      ];
    }

    return $renderArray;
  }
}
