<?php

/**
 * @file
 * Contains \Drupal\bitbucket_connector\Form\PipelineDeleteForm.
 */

namespace Drupal\bitbucket_connector\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @ingroup dictionary
 */
class PipelineDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete entity %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.bitbucket_pipeline.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('bitbucket_connector')->notice('deleted %title.',
      array(
        '%title' => $this->entity->label(),
      ));

    $form_state->setRedirect('entity.bitbucket_pipeline.collection');
  }

}
