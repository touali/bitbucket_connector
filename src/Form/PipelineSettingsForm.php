<?php

/**
 * @file
 * Contains \Drupal\bitbucket_connector\Form\PipelineSettingsForm.
 */

namespace Drupal\bitbucket_connector\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @ingroup dictionary
 */
class PipelineSettingsForm extends FormBase {
  /**
   * @return string
   */
  public function getFormId() {
    return 'bitbucket_pipeline_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['bitbucket_pipeline_settings']['#markup'] = 'Settings form for Bitbucket pipeline. Manage field settings here.';
    return $form;
  }
}
