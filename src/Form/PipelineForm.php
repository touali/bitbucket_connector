<?php
/**
 * @file
 * Contains Drupal\bitbucket_connector\Form\PipelineForm.
 */

namespace Drupal\bitbucket_connector\Form;

use Drupal\bitbucket_connector\Entity\PipelineEntity;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * @ingroup bitbucket_connector
 */
class PipelineForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity PipelineEntity */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.bitbucket_pipeline.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
}
