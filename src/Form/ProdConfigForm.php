<?php
/**
 * @file
 * Contains Drupal\bitbucket_connector\Form\ProdConfigForm.
 */

namespace Drupal\bitbucket_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ProdConfigForm
 * @package Drupal\bitbucket_connector\Form
 */
class ProdConfigForm extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return 'bitbucket_connector.settings.prod';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'bitbucket_connector__settings__prod';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config($this->getEditableConfigNames());

    $form['auth'] = [
      '#type' => 'radios',
      '#title' => $this->t('Authorization'),
      '#options' => [
        'basic' => $this->t('Basic'),
        'oauth' => $this->t('OAuth 2.0')
      ],
      '#default_value' => $config->get('auth') ? $config->get('auth') : 'basic',
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id') ? $config->get('client_id') : '',
    ];

    $form['client_secret'] = [
      '#type' => 'password',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret') ? $config->get('client_secret') : '',
    ];

    $form['workspace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Workspace'),
      '#default_value' => $config->get('workspace') ? $config->get('workspace') : 'isics',
    ];

    $form['repository'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Repository'),
      '#default_value' => $config->get('repository') ? $config->get('repository') : '',
    ];

    $form['branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Branch'),
      '#default_value' => $config->get('branch') ? $config->get('branch') : 'develop',
    ];

    $form['pipeline_custom_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pipeline custom name'),
      '#default_value' => $config->get('pipeline_custom_name') ? $config->get('pipeline_custom_name') : 'deployment-front-app-prod',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    parent::submitForm($form, $form_state);

    $config = $this->configFactory->getEditable($this->getEditableConfigNames());

    $config
      ->set('auth', $form_state->getValue('auth'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('workspace', $form_state->getValue('workspace'))
      ->set('repository', $form_state->getValue('repository'))
      ->set('branch', $form_state->getValue('branch'))
      ->set('pipeline_custom_name', $form_state->getValue('pipeline_custom_name'))
      ->save();
  }
}
