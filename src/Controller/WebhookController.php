<?php

namespace Drupal\bitbucket_connector\Controller;

use Drupal\bitbucket_connector\Entity\PipelineEntity;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PublishController
 * @package Drupal\bitbucket_connector\Controller
 */
class WebhookController extends ControllerBase
{
  /** @var EntityStorageInterface $pipelineStorage */
  protected $pipelineStorage;

  /** @var MessengerInterface $messenger */
  protected $messenger;

  /**
   * WebhookController constructor.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct() {
    $this->pipelineStorage = \Drupal::entityTypeManager()->getStorage('bitbucket_pipeline');
    $this->messenger = \Drupal::messenger();
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildStatus(Request $request) {
    $now = DrupalDateTime::createFromTimestamp(time());
    $now->setTimezone(new \DateTimeZone('UTC'));
    $url = Url::fromRoute('<front>');
    $response = new RedirectResponse($url->toString());
    $body = json_decode($request->getContent());

    if (!$body) {
      return $response;
    }

    $url = $body->commit_status->url;
    $status = $body->commit_status->state;
    $pipelineId = end(explode('/', $url));

    $entityIds = $this->getPipelineEntityByPipelineId($pipelineId);

    // Pipeline entity update
    if ($entityIds && $status !== PipelineEntity::STATUS_IN_PROGRESS) {
      $entityId = reset($entityIds);
      $entity = $this->pipelineStorage->load((int) $entityId);
      $entity->set('status', $status);
      $entity->set('end_time', $now->format('Y-m-d\TH:i:s'));

      switch ($status) {
        case PipelineEntity::STATUS_STOPPED:
          $this->messenger->addWarning(t('Your publication has been stopped, only one publication at a time is allowed.'));
          break;
        case PipelineEntity::STATUS_FAILED:
          $this->messenger->addError(t('Your publication has failed, please contact an administrator.'));
          break;
        case PipelineEntity::STATUS_SUCCESSFUL:
          $this->messenger->addMessage(t('Your publication went smoothly, do not hesitate to reload your site to see your changes.'));
          break;
      }

      $entity->save();
    }

    return $response;
  }

  /**
   * @param $pipelineId
   *
   * @return array|int
   */
  protected function getPipelineEntityByPipelineId($pipelineId) {
    $entityQuery = \Drupal::entityQuery('bitbucket_pipeline')
      ->condition('pipeline_id', $pipelineId);

    return $entityQuery->execute();
  }
}

