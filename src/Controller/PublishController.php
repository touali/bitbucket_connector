<?php

namespace Drupal\bitbucket_connector\Controller;

use Drupal\bitbucket_connector\Entity\PipelineEntity;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PublishController
 * @package Drupal\bitbucket_connector\Controller
 */
class PublishController extends ControllerBase
{
  /** @var ConfigFactoryInterface $configFactory */
  protected $configFactory;

  /** @var EntityStorageInterface $pipelineStorage */
  protected $pipelineStorage;

  /** @var Client $client */
  protected $client;

  /** @var string $token */
  protected $token;

  /** @var string */
  const OAUTH_TOKEN = 'https://bitbucket.org/site/oauth2/access_token';

  /** @var string */
  const BASIC_AUTH_URL = 'https://api.bitbucket.org/2.0/repositories/%s/%s/pipelines/';

  /** @var array */
  const REQUIRED_CONFIGURATION_DATA = [
    'auth',
    'client_id',
    'client_secret',
    'workspace',
    'repository',
    'branch',
    'pipeline_custom_name',
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory)
  {
    $this->configFactory = $config_factory;
    $this->pipelineStorage = \Drupal::entityTypeManager()->getStorage('bitbucket_pipeline');
    $this->client = \Drupal::httpClient();
    $this->token = '';
  }

  /**
   * @param $env
   */
  public function publish($env)
  {
    $now = DrupalDateTime::createFromTimestamp(time(), new \DateTimeZone('UTC'));
    $url = Url::fromRoute('view.pipelines.page');
    $response = new RedirectResponse($url->toString());
    $messenger = \Drupal::messenger();
    $data = $this->getFormData($env);

    // Check if env is configured
    if (!$this->checkEnvConfiguration($data)) {
      $messenger->addWarning(
        new TranslatableMarkup('This environment is not properly configured. Please contact an administrator.')
      );

      $response->send();
    }

    // Check auth Method
    if ($data['auth'] === 'oauth') {
      $this->token = $this->getAuthToken($data['client_id'], $data['client_secret']);
    }

    // Publish
    if ($responseData = $this->sendPublishRequest(
      $data["client_id"],
      $data["client_secret"],
      $data["workspace"],
      $data["repository"],
      $data["branch"],
      $data["pipeline_custom_name"]
    )) {
      // Pipeline entity creation
      $entity = $this->pipelineStorage->create([
        'pipeline_id' => $responseData->build_number,
        'status' => PipelineEntity::STATUS_IN_PROGRESS,
        'environment' => $data["branch"] === 'develop' ? PipelineEntity::ENV_PREPRODUCTION : PipelineEntity::ENV_PRODUCTION,
        'start_time' => $now->format('Y-m-d\TH:i:s'),
        'end_time' => null
      ]);

      $entity->save();

      // Flash message
      $messenger->addMessage(new TranslatableMarkup('Your %env release update has been scheduled.'), [
        '%env' => $env
      ]);
    } else {
      $messenger->addError(
        new TranslatableMarkup('An error occurred during the publishing process. Please contact an administrator.')
      );
    }

    $response->send();
  }

  /**
   * @param $env
   * @return Config
   */
  protected function getFormData($env) {
    $data = $env === 'prod'
      ? $this->configFactory->getEditable('bitbucket_connector.settings.prod')
      : $this->configFactory->getEditable('bitbucket_connector.settings.preprod');

    $data = $data->getRawData();

    $data = array_filter($data, function ($item) {
      return !empty($item);
    });

    return $data;
  }

  /**
   * @param $data
   * @return bool
   */
  protected function checkEnvConfiguration($data) {
    return count(array_intersect_key(array_flip(self::REQUIRED_CONFIGURATION_DATA), $data)) === count(self::REQUIRED_CONFIGURATION_DATA);
  }

  /**
   * @param $clientID
   * @param $clientSecret
   * @return false|mixed
   */
  protected function getAuthToken($clientID, $clientSecret)
  {
    try {
      $response = $this->client->post(self::OAUTH_TOKEN, [
        'form_params' => [
          'grant_type' => 'client_credentials',
          'client_id' => $clientID,
          'client_secret' => $clientSecret,
        ],
      ]);

      $content = json_decode($response->getBody()->getContents());

      return $content->access_token;
    } catch (RequestException $e) {
      return false;
    }
  }

  /**
   * @param $clientID
   * @param $clientSecret
   * @param $workspace
   * @param $repo
   * @param $branch
   * @return false|mixed
   */
  protected function sendPublishRequest($clientID, $clientSecret, $workspace, $repo, $branch, $pipelineCustomName)
  {
    $uri = sprintf(self::BASIC_AUTH_URL, $workspace, $repo);

    $options = [
      "headers" => [
        "Content-Type" => "application/json",
      ],
      "json" => [
        "target" => [
          "ref_type" => "branch",
          "type" => "pipeline_ref_target",
          "ref_name" => $branch,
          "selector" => [
            "type" => "custom",
            "pattern" => $pipelineCustomName
          ]
        ]
      ]
    ];

    if ($this->token) {
      $options["headers"]["Authorization"] = "Bearer {$this->token}";
    } else {
      $options["auth"] = [$clientID, $clientSecret];
    }

    try {
      $response = $this->client->post($uri, $options);

      return json_decode($response->getBody()->getContents());
    } catch (RequestException $e) {
      return false;
    }
  }
}

