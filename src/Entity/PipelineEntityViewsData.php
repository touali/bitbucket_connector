<?php

namespace Drupal\bitbucket_connector\Entity;

use Drupal\views\EntityViewsData;

/**
 * Class PipelineEntityViewsData
 *
 * @package Drupal\bitbucket_connector\Entity
 */
class PipelineEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
