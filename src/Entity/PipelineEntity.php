<?php

namespace Drupal\bitbucket_connector\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Bitbucket pipeline entity.
 *
 * @ingroup bitbucket_connector
 *
 * @ContentEntityType(
 *   id = "bitbucket_pipeline",
 *   label = @Translation("Pipeline"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\bitbucket_connector\Entity\PipelineEntityListBuilder",
 *     "views_data" = "Drupal\bitbucket_connector\Entity\PipelineEntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\bitbucket_connector\Form\PipelineForm",
 *       "edit" = "Drupal\bitbucket_connector\Form\PipelineForm",
 *       "delete" = "Drupal\bitbucket_connector\Form\PipelineDeleteForm",
 *     }
 *   },
 *   base_table = "bitbucket_pipeline",
 *   admin_permission = "administer bitbucket_pipeline entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "user_id" = "user_id",
 *     "created" = "created",
 *     "changed" = "changed",
 *     "environment" = "environment",
 *     "start_time" = "start_time",
 *   },
 *   links = {
 *     "canonical" = "/bitbucket_pipeline/{bitbucket_pipeline}",
 *     "edit-form" = "/bitbucket_pipeline/{bitbucket_pipeline}/edit",
 *     "delete-form" = "/bitbucket_pipeline/{bitbucket_pipeline}/delete",
 *     "collection" = "/bitbucket_pipeline/list"
 *   },
 *   field_ui_base_route = "entity.bitbucket_pipeline.bitbucket_pipeline_settings",
 * )
 */
class PipelineEntity extends ContentEntityBase implements ContentEntityInterface
{
  use EntityChangedTrait;

  const ENV_PRODUCTION = 'prod',
    ENV_PREPRODUCTION = 'preprod';

  const STATUS_FAILED = 'FAILED',
    STATUS_IN_PROGRESS = 'INPROGRESS',
    STATUS_STOPPED = 'STOPPED',
    STATUS_SUCCESSFUL = 'SUCCESSFUL';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['pipeline_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Pipeline ID'))
      ->setDescription(t('The pipeline ID.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setStorageRequired(true)
      ->setRequired(true)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 0
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['environment'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Environment'))
      ->setDescription(t('The environment of the Bitbucket pipeline entity.'))
      ->setSettings([
        'allowed_values' => [
          self::ENV_PRODUCTION => t('Production'),
          self::ENV_PREPRODUCTION => t('Preproduction')
        ]
      ])
      ->setDefaultValue('prod')
      ->setStorageRequired(true)
      ->setRequired(true)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDescription(t('The environment of the Bitbucket pipeline entity.'))
      ->setSettings([
        'allowed_values' => [
          self::STATUS_FAILED => t('Failed'),
          self::STATUS_IN_PROGRESS => t('In progress'),
          self::STATUS_STOPPED => t('Stopped'),
          self::STATUS_SUCCESSFUL => t('Successful')
        ]
      ])
      ->setDefaultValue('INPROGRESS')
      ->setStorageRequired(true)
      ->setRequired(true)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['start_time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Start time'))
      ->setDescription(t('The start time of the Bitbucket pipeline.'))
      ->setSettings([
        'datetime_type' => 'datetime'
      ])
      ->setDefaultValue(null)
      ->setStorageRequired(true)
      ->setRequired(true)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['end_time'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('End time'))
      ->setDescription(t('The end time of the Bitbucket pipeline.'))
      ->setSettings([
        'datetime_type' => 'datetime'
      ])
      ->setDefaultValue(null)
      ->setStorageRequired(false)
      ->setRequired(false)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
}

