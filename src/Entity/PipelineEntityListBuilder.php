<?php

/**
 * @file
 * Contains \Drupal\bitbucket_connector\Entity\EntityListBuilder.
 */

namespace Drupal\bitbucket_connector\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ingroup bitbucket_connector
 */
class PipelineEntityListBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * @param EntityTypeInterface $entity_type
   * @param EntityStorageInterface $storage
   * @param UrlGeneratorInterface $url_generator
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['pipeline_id'] = $this->t('Pipeline ID');
    $header['environment'] = $this->t('Environment');
    $header['status'] = $this->t('Status');
    $header['start_time'] = $this->t('Start time');
    $header['end_time'] = $this->t('End time');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->get('pipeline_id')->value;
    $row['environment'] = $entity->get('environment')->value;
    $row['status'] = $entity->get('status')->value;
    $row['start_time'] = $entity->get('start_time')->date->format('d/m/Y - H:i');
    $row['end_time'] = $entity->get('end_time')->date
      ? $entity->get('end_time')->date->format('d/m/Y - H:i')
      : null;

    return $row + parent::buildRow($entity);
  }
}
