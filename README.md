# Bitbucket Connector

A Drupal 8/9 module for the _Drusby_ Stack (Drupal + Gatsby) to deploy our project via Bitbucket pipelines.

## Disclaimer

__This project is in progress, most of the features are there but there is still a lot of work to do__

### Implemented

* Env config
* Pipeline trigger

### WIP

* Pipeline Custom Entity

### TODO

* Pipeline status endpoint
* Pipeline entity creation on trigger
* Pipeline entity update on status change
* Form default value improvement (yml config instead of static value)
* Translations (fr_FR)
* Code review & cleanup

## Requirements

* Drupal 8+
* Composer
* Git

## Installation

Add to the _repositories_ array in your _composer.json_
__Note: use the https url__

    {
        "type": "vcs",
        "url": "https://touali@bitbucket.org/touali/bitbucket_connector.git"
    }

Then type in your terminal

````composer require touali/bitbucket_connector````
